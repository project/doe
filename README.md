# Dynamic Options Element
This Drupal module simplifies the configuration of options for select lists, checkboxes, and radio buttons. It offers a straightforward approach, avoiding the use of complex "key|value" pairs. Developers can effortlessly integrate dynamic options elements into their modules using $element['#type'] = 'options_element'. Even if JavaScript is disabled, configuration through a standard text area remains feasible.

**Features**
Basic functionality: Simplifies the setup of options for select lists, checkboxes, and radio buttons.
Unique features: Provides an intuitive method for developers to enhance their modules with dynamic options.
Use cases: Ideal for developers aiming to streamline the user experience of Drupal sites by offering customizable and accessible options.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/doe).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/doe).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

In this project no additional dependencies are needed.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

After installing this module, developers can easily integrate dynamic options elements into their own modules using the code $element['#type'] = 'options_element'. Even if JavaScript is not functioning, it still allows configuration using a standard text area.


## Maintainers

- Alejandro Aarnau - [aarnau](https://www.drupal.org/u/aarnau)
- Mahamadu Sillah - [msillah](https://www.drupal.org/u/msillah)